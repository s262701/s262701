import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

import db.CloudantClientBase;
import db.CloudantClientFactory;


public class Main {

	private static Logger log = LogManager.getLogger("logger");
	
	private static String DATABASE_NAME = "sample_shopping_account";
	
	public static void main(String[] args) throws Exception {
		log.info("******************** MAIN 시작 ********************");
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		
		saveTest(db);
		
//		updateTest(db);
		
//		findDataTest(db);
		
//		findDataWithKeyTest(db);
		
		
		log.info("******************** MAIN 종료 ********************");
	}
	
	
	public static void saveTest(CloudantClientBase db) throws Exception {
//		JsonObject jo = new JsonObject();
//		jo.addProperty("type", "test");
//		jo.addProperty("name", "save test");
//		
//		String createdId = db.save(DATABASE_NAME, jo);
//		log.info(String.format("created : %s", createdId));
		
		TestModel test = new TestModel();
		test.setType("test");
		test.setName("save test");
		
		String createdId = db.save2(DATABASE_NAME, test);
		log.info(String.format("created : %s", createdId));
	}
	
	public static void updateTest(CloudantClientBase db) throws Exception {
		List<JsonObject> values = db.getValuesWithKey(DATABASE_NAME, "test", "getTest", "b80d5a238eee41e997402d68485b863e");
		if (!values.isEmpty()) {
			JsonObject jo = values.get(0);
			jo.addProperty("name", "update test");
			
			String updatedId = db.update(DATABASE_NAME, jo);
			log.info(String.format("updated : %s", updatedId));
		}
	}
	
	public static void findDataTest(CloudantClientBase db) throws Exception {
		List<JsonObject> values = db.getValues(DATABASE_NAME, "test", "getTest");
		if (!values.isEmpty()) {
			for (JsonObject jo : values) {
				log.info(jo.toString());
			}
		}
	}
	
	public static void findDataWithKeyTest(CloudantClientBase db) throws Exception {
		List<JsonObject> values = db.getValuesWithKey(DATABASE_NAME, "test", "getTest", "b80d5a238eee41e997402d68485b863e");
		if (!values.isEmpty()) {
			JsonObject jo = values.get(0);
			log.info(jo.toString());
		}
	}
}

class TestModel {
	private String _id;
	private String _rev;
	private String type;
	private String name;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String get_rev() {
		return _rev;
	}
	public void set_rev(String _rev) {
		this._rev = _rev;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
