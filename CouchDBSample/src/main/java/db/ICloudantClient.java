package db;

import java.net.MalformedURLException;

import com.cloudant.client.api.CloudantClient;

public interface ICloudantClient {
	
	public CloudantClient connect() throws MalformedURLException;
	
}
