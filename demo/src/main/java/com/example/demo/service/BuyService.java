package com.example.demo.service;

import java.util.List;

public interface BuyService {
	
	boolean buyProduct(String strJsonBuy) throws Exception;

	List<Object> getCustomBuyList(String strCustomerId) throws Exception;
	
	List<Object> getBuyList() throws Exception;
	
}
