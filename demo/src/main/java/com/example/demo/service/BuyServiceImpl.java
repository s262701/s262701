package com.example.demo.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.BuyDAO;
import com.example.demo.domain.Buy;
import com.example.demo.domain.Product;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.example.demo.service.ProductService;

@Service("buyService")
public class BuyServiceImpl implements BuyService {

	@Autowired
	private BuyDAO buyDao;
	
	@Autowired
	private ProductService productService;
	
	@Override
	public boolean buyProduct(String strJsonBuy) throws Exception {
		// TODO Auto-generated method stub
		if(strJsonBuy == null) {return false;}
		Gson gson = new Gson();
		
		Buy buy = gson.fromJson(strJsonBuy, Buy.class);
		if(buy == null) {return false;}
		if(buy.getQuantity() <= 0) {return false;}
		Product product = productService.getProduct(buy.getProductId());
		if(product == null) {return false;}
		if(buy.getQuantity() > product.getStock()) {return false;}
		product.setStock(product.getStock() - buy.getQuantity());

		if(product.getStock() == 0) {product.setDeleted(true);}
		
		String productToString = gson.toJson(product, Object.class);
		if(productToString == null) {return false;}
		productService.updateProduct(productToString);
		
		boolean boughtProduct = buyDao.buyProduct(buy);
		
		return boughtProduct;
	}

	@Override
	public List<Object> getCustomBuyList(String strCustomerId) throws Exception {
		// TODO Auto-generated method stub
		if(strCustomerId == null) {return null;}
		Gson gson = new Gson();
		
		List<JsonObject> li = buyDao.getCustomBuyList(strCustomerId);
		if(li.isEmpty()) {return null;}
		List<Buy> lib = new ArrayList<Buy>();		
		List<Object> lio = new ArrayList<>();
		for (int i = 0; i < li.size(); i++) {
			Buy buy = gson.fromJson(li.get(i), Buy.class);
			
			lib.add(buy);
		}
		lib.sort(new Comparator<Buy>() {
			@Override
			public int compare(Buy b1, Buy b2) {
				// TODO Auto-generated method stub
				
				return b1.getName().compareTo(b2.getName());
			}
		});
		
		for (int i = 0; i < lib.size(); i++) {
			String strBuy = gson.toJson(lib.get(i));
			Buy buy = gson.fromJson(strBuy, Buy.class);
			
			lio.add(buy);
		}
		return lio;
	}

	@Override
	public List<Object> getBuyList() throws Exception {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		List<JsonObject> li = buyDao.getBuyList();
		if(li.isEmpty()) {return null;}
		List<Object> lio = new ArrayList<>();
		
		for (int i = 0; i < li.size(); i++) {
			Buy buy = gson.fromJson(li.get(i), Buy.class);
			
			boolean checkDuplicate = false;
			for (int j = 0; j < i; j++) {
				if(lio.size() <= j) {continue;}
				Buy checkProduct = (Buy) lio.get(j);
				if(checkProduct.getProductId().equals(buy.getProductId())){
					checkProduct.setQuantity(checkProduct.getQuantity() + buy.getQuantity());
					checkProduct.setPaid(checkProduct.getPaid() + buy.getPaid());
					lio.set(j, checkProduct);
					checkDuplicate = true;
				}
			}
			if(!checkDuplicate) {
				Product product = productService.getProduct(buy.getProductId());
				
				buy.setStock(product.getStock());
				
				lio.add(buy);
			}
		}
		return lio;
	}

}
