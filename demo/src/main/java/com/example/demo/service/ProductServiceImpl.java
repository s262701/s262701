package com.example.demo.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ProductDAO;
import com.example.demo.domain.Product;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Service("productService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDao;
	
	@Override
	public boolean saveProduct(String strJsonProduct) throws Exception {
		// TODO Auto-generated method stub
		if(strJsonProduct == null) {return false;}
		
		Gson gson = new Gson();
		JsonObject jo = gson.fromJson(strJsonProduct, JsonObject.class);
		if(jo == null) {return false;}
		Product product = gson.fromJson(jo, Product.class);
		if(product.getName() == null) {return false;}
		if(product.getInfo() == null) {return false;}
		if(product.getAmount() <= 0) {return false;}
		if(product.getPrice() < 0) {return false;}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		product.setDate(format.format(System.currentTimeMillis()));
		
		return productDao.newProduct(product);
	}

	@Override
	public boolean updateProduct(String strJsonProduct) throws Exception {
		// TODO Auto-generated method stub
		if(strJsonProduct == null) {return false;}
		
		Gson gson = new Gson();
		JsonObject jo = gson.fromJson(strJsonProduct, JsonObject.class);
		if(jo == null) {return false;}
		List<JsonObject> li = productDao.getProduct(jo.get("_id").getAsString());
		if(li.isEmpty()) {return false;}
		Product product = new Product();
		
		for (int i = 0; i < li.size(); i++) {
			product = gson.fromJson(li.get(i), Product.class);
		}
		
		if(jo.get("name") != null) {
			product.setName(jo.get("name").getAsString());
		}
		if(jo.get("info") != null) {
			product.setInfo(jo.get("info").getAsString());
		}
		if(jo.get("price") != null) {
			product.setPrice(jo.get("price").getAsInt());
		}
		if(jo.get("stock") != null) {
			product.setStock(jo.get("stock").getAsInt());
		}
		if(jo.get("deleted") != null) {
			product.setDeleted(jo.get("deleted").getAsBoolean());
		}
		
		String objectToString = gson.toJson(product, Object.class);
		if(objectToString == null) {return false;}
		JsonObject stringToJo = gson.fromJson(objectToString, JsonObject.class);
		if(stringToJo == null) {return false;}
		
		return productDao.updateProduct(stringToJo);
	}

	@Override
	public Map<String, Object> getProductList(String skip, String limit) throws Exception {
		// TODO Auto-generated method stub
		if(skip == null || limit == null) {return null;}
		Gson gson = new Gson();
		
		long allCount = productDao.checkCount("product");
		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("count", allCount);
		
		List<JsonObject> li = productDao.checkType(Long.parseLong(skip), Integer.parseInt(limit));
		
		if(li.isEmpty()) {return null;}
		List<Object> limo = new ArrayList<>();
		
		for (int i = 0; i < li.size(); i++) {
			Product product = new Product();
			product = gson.fromJson(li.get(i), Product.class);
						
			limo.add(product);
		}
		responseMap.put("data", limo);
		responseMap.put("call", "productList");
		
		return responseMap;
	}

	@Override
	public Product getProduct(String strProduct_id) throws Exception {
		// TODO Auto-generated method stub
		if(strProduct_id == null) {return null;}
		Gson gson = new Gson();
		List<JsonObject> li = productDao.getProduct(strProduct_id);
		if(li.isEmpty()) {return null;}
		
		Product product = new Product();
		
		for (int i = 0; i < li.size(); i++) {
			product = gson.fromJson(li.get(i), Product.class);			
		}
		
		return product;
	}

	@Override
	public Map<String, Object> getSearchList(String strProductName, String skip, String limit) throws Exception {
		// TODO Auto-generated method stub
		if(strProductName == null || skip == null || limit == null) {return null;}
		Gson gson = new Gson();
		List<Object> lio = new ArrayList<>();
		
		Map<String, Object> responseMap = new HashMap<String, Object>();
		
		if(!strProductName.isEmpty()) {
			List<JsonObject> li = productDao.searchProduct(strProductName, Long.parseLong(skip), Integer.parseInt(limit));
			
			for (int i = 0; i < li.size(); i++) {
				Product product = new Product();
				product = gson.fromJson(li.get(i), Product.class);		
				
				lio.add(product);
			}
			responseMap.put("data", lio);
		} else {
			return null;
		}
		responseMap.put("call", "searchList");
		
		return responseMap;
	}

}
