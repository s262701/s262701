package com.example.demo.service;

import java.util.Map;

import com.example.demo.domain.Product;

public interface ProductService {

	boolean saveProduct(String strJsonProduct) throws Exception;
	
	boolean updateProduct(String strJsonProduct) throws Exception;
	
	Map<String, Object> getProductList(String skip, String limit) throws Exception;
	
	Product getProduct(String strProuct_id) throws Exception;
	
	Map<String, Object> getSearchList(String strProductName, String skip, String limit) throws Exception;
}
