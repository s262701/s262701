package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.CustomerDAO;
import com.example.demo.domain.Customer;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDao;

	@Override
	public boolean checkCustomerId(String id) throws Exception {
		// TODO Auto-generated method stub
		if(id == null) {return true;}
		List<JsonObject> li = customerDao.checkId(id);
		if(!li.isEmpty()) {return false;}
		
		return true;
	}

	@Override
	public boolean saveCustomer(String strJson) throws Exception {
		// TODO Auto-generated method stub
		if(strJson == null) {return false;}
		Gson gson = new Gson();
		JsonObject jo = gson.fromJson(strJson, JsonObject.class);
		if(jo == null) {return false;}
		Customer customer = new Customer();
		if(jo.get("type") == null) {return false;}
		customer.setType(jo.get("type").getAsString());
		if(jo.get("signinId") == null) {return false;}
		customer.setId(jo.get("signinId").getAsString());
		if(jo.get("signinPassword") == null) {return false;}
		customer.setPassword(jo.get("signinPassword").getAsString());
		if(jo.get("signinPhone") == null) {return false;}
		customer.setPhone(jo.get("signinPhone").getAsInt());
		if(jo.get("signinAddress") == null) {return false;}
		customer.setAddress(jo.get("signinAddress").getAsString());
		
		return customerDao.signin(customer);
	}

	public String loginCustomer(String strJson) throws Exception {
		// TODO Auto-generated method stub
		if(strJson == null) {return "";}
		Gson gson = new Gson();
		JsonObject jo = gson.fromJson(strJson, JsonObject.class);
		if(jo == null) {return "";}
		Customer customer = new Customer();
		if(jo.get("loginId") == null) {return "";}
		customer.setId(jo.get("loginId").getAsString());
		if(jo.get("loginPassword") == null) {return "";}
		customer.setPassword(jo.get("loginPassword").getAsString());
		
		List<JsonObject> li = customerDao.checkId(customer.getId());
		
		if(li.isEmpty()) {return "";}
		
		if(li.get(0).get("password").getAsString() == null) {return "";}
		if(li.get(0).get("password").getAsString().equals(customer.getPassword())) {
			return li.get(0).get("id").getAsString();
		}
		return "";
	}
}
