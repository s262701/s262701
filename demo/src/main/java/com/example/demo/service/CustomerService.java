package com.example.demo.service;


public interface CustomerService {
	
	boolean checkCustomerId(String id) throws Exception;
	
	boolean saveCustomer(String strJsonCustomer) throws Exception;

	String loginCustomer(String strJsonCustomer) throws Exception;
	
}
