package com.example.demo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.gson.JsonObject;
import com.example.demo.db.CloudantClientFactory;
import com.example.demo.domain.Customer;
import com.example.demo.db.CloudantClientBase;

@Repository
public class CustomerDAO {

	private static String DATABASE_NAME = "sample_shopping_account";

	public boolean signin(Customer customer) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		Boolean createdId = db.create(DATABASE_NAME, customer);
		
		return createdId;
	}
	
	public List<JsonObject> checkId(String customerId) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValuesWithKey(DATABASE_NAME, "customer", "getCheckedId", customerId);
		
		return values;
	}
}
