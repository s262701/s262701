package com.example.demo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.db.CloudantClientBase;
import com.example.demo.db.CloudantClientFactory;
import com.example.demo.domain.Buy;
import com.google.gson.JsonObject;

@Repository
public class BuyDAO {

	private static String DATABASE_NAME = "sample_shopping_account";
	
	public boolean buyProduct(Buy buy) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		Boolean boughtProduct = db.create(DATABASE_NAME, buy);
		
		return boughtProduct;
	}
	
	public List<JsonObject> getCustomBuyList(String id) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValuesWithKey(DATABASE_NAME, "buy", "getCustomBuyList", id);
		
		return values;
	}
	
	public List<JsonObject> getBuyList() throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValues(DATABASE_NAME, "buy", "getBuyList");
		
		return values;
	}
	
}
