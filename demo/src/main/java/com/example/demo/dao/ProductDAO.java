package com.example.demo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.google.gson.JsonObject;
import com.example.demo.db.CloudantClientFactory;
import com.example.demo.domain.Product;
import com.example.demo.db.CloudantClientBase;

@Repository
public class ProductDAO {
	
	private static String DATABASE_NAME = "sample_shopping_account";
	
	public boolean newProduct(Product product) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		Boolean createdProduct = db.create(DATABASE_NAME, product);
		
		return createdProduct;
	}
	
	public List<JsonObject> checkType(long skip, int limit) throws Exception {
		
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValuesWithSkipLimit(DATABASE_NAME, "product", "getProductList", skip, limit);
		
		return values;
	}
	
	public long checkCount(String type) throws Exception {
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		long values = db.getCount(DATABASE_NAME, "product", "getProductList", type);
		
		return values;
	}
	
	public List<JsonObject> getProduct(String _id) throws Exception {
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValuesWithKey(DATABASE_NAME, "product", "getProduct", _id);
		
		return values;
	}
	
	public List<JsonObject> searchProduct(String name, long skip, int limit) throws Exception {
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");
		List<JsonObject> values = db.getValuesWithKeySkipLimit(DATABASE_NAME, "product", "searchProduct", name, skip, limit);
		
		return values;
	}
	
	public boolean updateProduct(JsonObject stringToJo) throws Exception {
		CloudantClientBase db = CloudantClientFactory.getCloudantClient("local", "sg");	
		JsonObject jo = stringToJo;
	
		String updatedId = db.update(DATABASE_NAME, jo);
		
		if(updatedId != "") {return true;}
		
		return false;
	}
}
