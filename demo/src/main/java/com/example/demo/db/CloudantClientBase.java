package com.example.demo.db;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.api.model.Response;
import com.cloudant.client.api.views.Key;
import com.cloudant.client.api.views.ViewResponse;
import com.google.gson.JsonObject;

public class CloudantClientBase implements ICloudantClient {
	
	protected Logger log = LogManager.getLogger("logger");
	
	protected CloudantClient client;
	
	
	public CloudantClientBase() throws Exception {
		this.client = connect();
	}

	public CloudantClient connect() throws MalformedURLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public CloudantClient getClient() {
		return client;
	}
	
	public List<JsonObject> getValues(String dbName, String designDoc, String viewName) throws IOException {
		Database database = client.database(dbName, false);
		ViewResponse<String, JsonObject> vrb = database.getViewRequestBuilder(designDoc, viewName)
				.newRequest(Key.Type.STRING, JsonObject.class)
				.build()
				.getResponse();
		
		return vrb.getValues();
	}
	
	public List<JsonObject> getValuesWithKey(String dbName, String designDoc, String viewName, String key) throws IOException {
		Database database = client.database(dbName, false);
		ViewResponse<String, JsonObject> vrb = database.getViewRequestBuilder(designDoc, viewName)
				.newRequest(Key.Type.STRING, JsonObject.class)
				.keys(key)
				.build()
				.getResponse();
		
		return vrb.getValues();
	}
	
	public List<JsonObject> getValuesWithKeySkipLimit(String dbName, String designDoc, String viewName, String key, long skip, int limit) throws IOException {
		Database database = client.database(dbName, false);
		ViewResponse<String, JsonObject> vrb = database.getViewRequestBuilder(designDoc, viewName)
				.newRequest(Key.Type.STRING, JsonObject.class)
				.descending(true)
				.keys(key)
				.skip(skip)
				.limit(limit)
				.build()
				.getResponse();
		
		return vrb.getValues();
	}

	public List<JsonObject> getValuesWithSkipLimit(String dbName, String designDoc, String viewName, long skip, int limit) throws IOException {
		Database database = client.database(dbName, false);
		ViewResponse<String, JsonObject> vrb = database.getViewRequestBuilder(designDoc, viewName)
				.newRequest(Key.Type.STRING, JsonObject.class)
				.descending(true)
				.skip(skip)
				.limit(limit)
				.build()
				.getResponse();
		
		return vrb.getValues();
	}
	
	public Long getCount(String dbName, String designDoc, String viewName, String key) throws IOException {
		Database database = client.database(dbName, false);
		ViewResponse<String, JsonObject> vrb = database.getViewRequestBuilder(designDoc, viewName)
				.newRequest(Key.Type.STRING, JsonObject.class)
				.keys(key)
				.build()
				.getResponse();
		
		return vrb.getTotalRowCount();
	}
	
	/*public String save(String dbName, JsonObject jo) {
		Database database = client.database(dbName, false);
		Response res = database.save(jo);
		if (res.getError() != null) {
			log.info(String.format("[ERROR] %s, id : %s", res.getReason(), res.getId()));
			return null;
		}
		return res.getId();
	}
	
	public String save2(String dbName, Object jo) {
		Database database = client.database(dbName, false);
		Response res = database.save(jo);
		if (res.getError() != null) {
			log.info(String.format("[ERROR] %s, id : %s", res.getReason(), res.getId()));
			return null;
		}
		return res.getId();
	}*/
	
	public Boolean create(String dbName, Object obj) {
		Database database = client.database(dbName, false);
		Response res = database.save(obj);
		if (res.getError() != null) {
			log.info(String.format("[ERROR] %s, id : %s", res.getReason(), res.getId()));
			return false;
		}
		return true;
	}
	
	public String update(String dbName, JsonObject jo) {
		Database database = client.database(dbName, false);
		Response res = database.update(jo);
		if (res.getError() != null) {
			log.info(String.format("[ERROR] %s, id : %s", res.getReason(), res.getId()));
			return null;
		}
		return res.getId();
	}

}
