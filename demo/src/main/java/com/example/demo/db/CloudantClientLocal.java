package com.example.demo.db;

import java.net.MalformedURLException;
import java.net.URL;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;

public class CloudantClientLocal extends CloudantClientBase {

	private static final String DB_URL = "http://192.168.134.12";
	private static final String DB_USER_NAME = "admin";
	private static final String DB_PASSWORD = "password";
	
	public CloudantClientLocal() throws Exception {
		super();
	}
	
	@Override
	public CloudantClient connect() throws MalformedURLException {
		log.info("DB URL : " + DB_URL);
		
		return ClientBuilder.url(new URL(DB_URL))
				.username(DB_USER_NAME)
				.password(DB_PASSWORD)
				.build();
	}
}
