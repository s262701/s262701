package com.example.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ProductService;


@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/createproduct", method = RequestMethod.POST)
	public boolean createProduct(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJsonProduct) throws Exception {
		if(strJsonProduct == null) {return false;}
		Boolean savedProduct = productService.saveProduct(strJsonProduct);
		
		return savedProduct;
	}
	
	@RequestMapping(value = "/productlist", method = RequestMethod.GET)
	public Map<String, Object> productList(String skip, String limit) throws Exception {
		if(skip == null) {skip = "0";}
		if(limit == null) {limit = "10";}
		Map<String, Object> productList = productService.getProductList(skip, limit);
		
		return productList;
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public Object product(String _id) throws Exception {
		if(_id == null) {return null;}
		Object product = productService.getProduct(_id);
		
		return product;
	}
	
	@RequestMapping(value = "/updateproduct", method = RequestMethod.PATCH)
	public boolean updateProduct(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJsonUpdateProduct) throws Exception {
		if(strJsonUpdateProduct == null) {return false;}
		boolean updatedProduct = productService.updateProduct(strJsonUpdateProduct);
		
		return updatedProduct;
	}
	
	@RequestMapping(value = "/searchproduct", method = RequestMethod.GET)
	public Map<String, Object> searchProduct(String name, String skip, String limit) throws Exception {
		if(name == null) {return null;}
		if(skip == null) {skip = "0";}
		if(limit == null) {limit = "10";}
		Map<String, Object> searchedProduct = productService.getSearchList(name, skip, limit);
		
		return searchedProduct;
	}
}
