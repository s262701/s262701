package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.CustomerService;


@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/checkid", method = RequestMethod.GET)
	public boolean getCustomer(String id) throws Exception {
		if(id == null) {return true;}
		boolean checkedCustomerId = customerService.checkCustomerId(id);
		
		return checkedCustomerId;
	}
	
	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public boolean createCustomer(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJsonCustomer) throws Exception {
		if(strJsonCustomer == null) {return false;}
		Boolean savedCustomer = customerService.saveCustomer(strJsonCustomer);
		
		return savedCustomer;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginCustomer(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJsonCustomer) throws Exception {
		if(strJsonCustomer == null) {return "";}
		String loginedId = customerService.loginCustomer(strJsonCustomer);

		return loginedId;
	}
}
