package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.BuyService;

@RestController
public class BuyController {
	
	@Autowired
	private BuyService buyService2;

	@RequestMapping(value = "/buyproduct", method = RequestMethod.POST)
	public boolean buyProduct(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJsonBuy) throws Exception {
		if(strJsonBuy == null) {return false;}
		boolean boughtProduct = buyService2.buyProduct(strJsonBuy);
		
		return boughtProduct;
	}
	
	@RequestMapping(value = "/buycustomlist", method = RequestMethod.GET)
	public List<Object> buyCustomList(String id) throws Exception {
		if(id == null) {return null;}
		List<Object> boughtCustomList = buyService2.getCustomBuyList(id);
		
		return boughtCustomList;
	}
	
	@RequestMapping(value = "/buylist", method = RequestMethod.GET)
	public List<Object> buyList() throws Exception {
		
		List<Object> boughtList = buyService2.getBuyList();
		
		return boughtList;
	}
}
