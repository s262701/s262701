// 플러그인 세팅
var gulp = require("gulp");
var fileinclude = require("gulp-file-include");
var webserver = require("gulp-webserver");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var minifyhtml = require("gulp-minify-html");
var sass = require("gulp-sass");
var livereload = require("gulp-livereload");
var browserSync = require("browser-sync").create();

// 경로
var src = "public/src";
var dist = "public/dist";

var paths = {
	js: src + "/js/*.js",
	scss: src + "/scss/*.scss",
	html: src + "/**/*.html"
};

// 웹 서버를 localhost:8000으로 실행
/*gulp.task("server", function() {
	return gulp.src(dist + "/login.html")
		.pipe(webserver());
});*/
/*gulp.task("server", function() {
	return browserSync.init({
		server: {
			baseDir: "./public/dist"
		}
	});
});*/

function server() {
	return browserSync.init({
		/*files: [
			"public/dist/*.html"
		],*/
		server: {
			baseDir: "./public/src",
			startPath: "/login.html"
		}
	});
};

// JavaScript파일을 하나로 합치고 압축
/*gulp.task("combine-js", function() {
	return gulp.src(paths.js)
		.pipe(concat("script.js"))
		.pipe(uglify())
		.pipe(gulp.dest(dist + "/js"));
});*/

// sass 파일을 css로 컴파일
/*gulp.task("compile-sass", function() {
	return gulp.src(paths.scss)
		.pipe(sass())
		.pipe(gulp.dest(dist + "/css"));
});*/

// HTML 파일 압축
/*gulp.task("compress-html", function() {
	return gulp.src(paths.html)
		.pipe(minifyhtml())
		.pipe(gulp.dest(dist + "/"))
		.pipe(browserSync.reload({stream:true}));
});*/

function compresshtml() {
	return gulp.src(paths.html)
		.pipe(minifyhtml())
		.pipe(gulp.dest(dist + "/"))
		.pipe(browserSync.reload({stream:true}));
};

// 파일 변경 감지 및 브라우저 재시작
/*gulp.task("watch", function() {
	livereload.listen();
	//gulp.watch(paths.js, ["combine-js"]);
	//gulp.watch(paths.scss, ["compile-sass"]);
	gulp.watch(paths.html, ["compress-html"]);
	gulp.watch(dist + "/**").on("change", livereload.changed);
});*/

function watch() {
	livereload.listen();
	//gulp.watch(paths.html, compresshtml);
	gulp.watch(src + "/**").on("change", livereload.changed);
};

/*function fileinclude() {
	gulp.src(["./public/dist/login.html", "./public.dist/*.html", {base: "./"}]).pipe(fileinclude({
			prefix: "@@",
			basepath: "@file"
	})).pipe(gulp.dest("./"));
};*/

// 기본 task 설정
gulp.task("default", gulp.series(/*compresshtml,*/ server));

gulp.task("watch", gulp.series(watch));

gulp.task("compresshtml", gulp.series(compresshtml));
